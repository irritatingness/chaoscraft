package com.irritatingness;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.FurnaceBurnEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTakeLecternBookEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.spigotmc.event.entity.EntityMountEvent;

import net.md_5.bungee.api.ChatColor;

public class ChaosCraftListener implements Listener {
	
	Plugin plugin;
	
	public ChaosCraftListener(Plugin plugin) {
		this.plugin = plugin;
	}
	
	private float getExplosion(float chance) {
		// Check if explosion will happen
		if (ThreadLocalRandom.current().nextFloat() <= chance) {
			// Explosion is happening, check what kind
			float explosionType = ThreadLocalRandom.current().nextFloat();
			float size = 1;
			if (explosionType <= ChaosCraft.LARGE_EXPLOSION_CHANCE) {
				// Large explosion
				size = (float) ThreadLocalRandom.current().nextInt(8, 12 + 1);
			} else if (explosionType <= ChaosCraft.MEDIUM_EXPLOSION_CHANCE) {
				// Medium explosion
				size = (float) ThreadLocalRandom.current().nextInt(4, 8 + 1);
			} else {
				// Small explosion
				size = (float) ThreadLocalRandom.current().nextInt(1, 4 + 1);
			}
			return size; 
		}
		else {
			return 0.0F;
		}
	}

	@EventHandler
	public void explosion(BlockExplodeEvent e) {
		if (ChaosCraft.CHAOS_MODE) {
			if (!e.blockList().isEmpty()) {
				float chance = ChaosCraft.CHAOS_EXPLOSION_CHANCE;
				float explosion = getExplosion(chance);

				Random rand = new Random();
				if (explosion != 0.0F) {
					List<Block> blockList = e.blockList();
					Location loc = blockList.get(rand.nextInt(blockList.size())).getLocation();
					this.plugin.getServer().getScheduler().runTaskLater(this.plugin, new ExplosionTimedRunnable(loc, explosion), 12);
				}
			}
		}
	}
	
	@EventHandler
	public void blockBreak(BlockBreakEvent e) {
		float chance = ChaosCraft.BLOCK_BREAK;
		float explosion = getExplosion(chance);
		Location loc = e.getBlock().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void blockPlace(BlockPlaceEvent e) {
		float chance = ChaosCraft.BLOCK_PLACE;
		float explosion = getExplosion(chance);
		Location loc = e.getBlock().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void mobSpawn(CreatureSpawnEvent e) {
		float chance = ChaosCraft.CREATURE_SPAWN;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void playerJoin(PlayerJoinEvent e) {
		float chance = ChaosCraft.PLAYER_JOIN;
		float explosion = getExplosion(chance);
		Location loc = e.getPlayer().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void sleepInBed(PlayerBedEnterEvent e) {
		float chance = ChaosCraft.SLEEP_BED;
		float explosion = getExplosion(chance);
		Location loc = e.getBed().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void playerDropItem(PlayerDropItemEvent e) {
		float chance = ChaosCraft.PLAYER_DROP_ITEM;
		float explosion = getExplosion(chance);
		Location loc = e.getItemDrop().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void bucketFill(PlayerBucketFillEvent e) {
		float chance = ChaosCraft.BUCKET_FILL;
		float explosion = getExplosion(chance);
		Location loc = e.getBlock().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void bucketEmpty(PlayerBucketEmptyEvent e) {
		float chance = ChaosCraft.BUCKET_EMPTY;
		float explosion = getExplosion(chance);
		Location loc = e.getBlock().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void playerRespawn(PlayerRespawnEvent e) {
		float chance = ChaosCraft.PLAYER_RESPAWN;
		float explosion = getExplosion(chance);
		Location loc = e.getPlayer().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, 12.0F, true, true);
			e.getPlayer().sendMessage(ChatColor.GOLD + "Congrats! You've just hit the 1% respawn explosion chance :)");
		}
	}
	
	@EventHandler
	public void animalBreed(EntityBreedEvent e) {
		float chance = ChaosCraft.ANIMAL_BREED;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}

	@EventHandler
	public void entityTeleport(EntityTeleportEvent e) {
		float chance = ChaosCraft.ENTITY_TELEPORT;
		float explosion = getExplosion(chance);
		// 50-50 on to/from location
		Location loc;
		float locDet = ThreadLocalRandom.current().nextFloat();
		if (locDet > 0.5F) {
			loc = e.getFrom();
		} else {
			loc = e.getTo();
		}
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void playerConsume(PlayerItemConsumeEvent e) {
		float chance = ChaosCraft.PLAYER_CONSUME;
		float explosion = getExplosion(chance);
		Location loc = e.getPlayer().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void playerItemBreak(PlayerItemBreakEvent e) {
		float chance = ChaosCraft.TOOL_BREAK;
		float explosion = getExplosion(chance);
		Location loc = e.getPlayer().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void shearEntity(PlayerShearEntityEvent e) {
		float chance = ChaosCraft.SHEAR_ENTITY;
		float explosion = getExplosion(chance);
		Location loc = e.getPlayer().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void playerFish(PlayerFishEvent e) {
		float chance = ChaosCraft.PLAYER_FISH;
		float explosion = getExplosion(chance);
		Location loc = e.getPlayer().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void playerEggThrow(PlayerEggThrowEvent e) {
		float chance = ChaosCraft.THROW_EGG;
		float explosion = getExplosion(chance);
		Location loc = e.getEgg().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void editBook(PlayerEditBookEvent e) {
		float chance = ChaosCraft.EDIT_BOOK;
		float explosion = getExplosion(chance);
		Location loc = e.getPlayer().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void enchantItem(EnchantItemEvent e) {
		float chance = ChaosCraft.ENCHANT_ITEM;
		float explosion = getExplosion(chance);
		Location loc = e.getEnchantBlock().getLocation();
		// Break the enchanting table block
		e.getEnchantBlock().breakNaturally();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void vehicleDestroy(VehicleDestroyEvent e) {
		float chance = ChaosCraft.VEHICLE_DESTROY;
		float explosion = getExplosion(chance);
		Location loc = e.getVehicle().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void portalCreate(PortalCreateEvent e) {
		float chance = ChaosCraft.CREATE_PORTAL;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void brewEvent(BrewEvent e) {
		float chance = ChaosCraft.BREW_POTION;
		float explosion = getExplosion(chance);
		Location loc = e.getBlock().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void entityDeath(EntityDeathEvent e) {
		float chance = ChaosCraft.ENTITY_DEATH;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void entityMount(EntityMountEvent e) {
		float chance = ChaosCraft.ENTITY_MOUNT;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void entityShootBow(EntityShootBowEvent e) {
		float chance = ChaosCraft.ENTITY_SHOOT_BOW;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void slimeSplit(SlimeSplitEvent e) {
		float chance = ChaosCraft.SLIME_SPLIT;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void fireworkExplode(FireworkExplodeEvent e) {
		float chance = ChaosCraft.FIREWORK_EXPLODE;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void itemDespawn(ItemDespawnEvent e) {
		float chance = ChaosCraft.ITEM_DESPAWN;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void villagerAcquireTrade(VillagerAcquireTradeEvent e) {
		float chance = ChaosCraft.VILLAGER_ACQUIRE_TRADE;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void furnaceBurn(FurnaceBurnEvent e) {
		float chance = ChaosCraft.FURNACE_BURN;
		float explosion = getExplosion(chance);
		Location loc = e.getBlock().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void leavesDecay(LeavesDecayEvent e) {
		float chance = ChaosCraft.LEAVES_DECAY;
		float explosion = getExplosion(chance);
		Location loc = e.getBlock().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void signChange(SignChangeEvent e) {
		float chance = ChaosCraft.SIGN_CHANGE;
		float explosion = getExplosion(chance);
		Location loc = e.getBlock().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void rightClickBlock(PlayerInteractEvent e) {
		// Only trigger on block right-click... even if this is PlayerInteractEvent.
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			float chance = ChaosCraft.RIGHT_CLICK_BLOCK;
			float explosion = getExplosion(chance);
			Location loc = e.getClickedBlock().getLocation();
			if (explosion != 0.0F) {
				loc.getWorld().createExplosion(loc, explosion, false, true);
			}
		}
	}
	
	@EventHandler
	public void playerTakeLecternBook(PlayerTakeLecternBookEvent e) {
		float chance = ChaosCraft.PLAYER_TAKE_LECTERN_BOOK;
		float explosion = getExplosion(chance);
		Location loc = e.getLectern().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
	
	@EventHandler
	public void playerLeashEntity(PlayerLeashEntityEvent e) {
		float chance = ChaosCraft.PLAYER_LEASH_ENTITY;
		float explosion = getExplosion(chance);
		Location loc = e.getEntity().getLocation();
		if (explosion != 0.0F) {
			loc.getWorld().createExplosion(loc, explosion, false, true);
		}
	}
}
