package com.irritatingness;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

import java.util.logging.Logger;

public class ChaosCraft extends JavaPlugin {

	public static boolean CHAOS_MODE;
	public static float CHAOS_EXPLOSION_CHANCE;
	public static float LARGE_EXPLOSION_CHANCE;
	public static float MEDIUM_EXPLOSION_CHANCE;
	public static float SMALL_EXPLOSION_CHANCE;
	public static float DEFAULT;
	public static float BLOCK_BREAK;
	public static float BLOCK_PLACE;
	public static float CREATURE_SPAWN;
	public static float PLAYER_JOIN;
	public static float SLEEP_BED;
	public static float PLAYER_DROP_ITEM;
	public static float BUCKET_FILL;
	public static float BUCKET_EMPTY;
	public static float PLAYER_RESPAWN;
	public static float ANIMAL_BREED;
	public static float ENTITY_TELEPORT;
	public static float PLAYER_CONSUME;
	public static float TOOL_BREAK;
	public static float SHEAR_ENTITY;
	public static float PLAYER_FISH;
	public static float THROW_EGG;
	public static float EDIT_BOOK;
	public static float ENCHANT_ITEM;
	public static float VEHICLE_DESTROY;
	public static float CREATE_PORTAL;
	public static float BREW_POTION;
	public static float ENTITY_DEATH;
	public static float ENTITY_MOUNT;
	public static float ENTITY_SHOOT_BOW;
	public static float SLIME_SPLIT;
	public static float FIREWORK_EXPLODE;
	public static float ITEM_DESPAWN;
	public static float VILLAGER_ACQUIRE_TRADE;
	public static float FURNACE_BURN;
	public static float LEAVES_DECAY;
	public static float SIGN_CHANGE;
	public static float RIGHT_CLICK_BLOCK;
	public static float PLAYER_TAKE_LECTERN_BOOK;
	public static float PLAYER_LEASH_ENTITY;
	
	public static Logger log;
	
	public void onEnable() {
		// Save default config
		this.saveDefaultConfig();
		
		// Load config values
		loadConfigValues();
		
		// Register listener
		this.getServer().getPluginManager().registerEvents(new ChaosCraftListener(this), this);
		// Register Logger
		log = this.getServer().getLogger();
	}
	
	// Handle commands
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ccreload")) {
			if (sender instanceof Player) {
				if (sender.hasPermission("chaoscraft.reload")) {
					this.loadConfigValues();
					sender.sendMessage(ChatColor.GREEN + "Configuration for ChaosCraft reloaded!");
					return true;
				}
			}
			else {
				this.loadConfigValues();
				sender.sendMessage("Configuration for ChaosCraft reloaded!");
				return true;
			}
		}
		return false;
	}
	
	private void loadConfigValues() {
		// Load config values
		CHAOS_MODE = this.getConfig().getBoolean("chaos_mode");
		CHAOS_EXPLOSION_CHANCE = (float) this.getConfig().getDouble("chaos_explosion_chance");
		LARGE_EXPLOSION_CHANCE = (float) this.getConfig().getDouble("explosion_size_chance.large");
		MEDIUM_EXPLOSION_CHANCE = (float) this.getConfig().getDouble("explosion_size_chance.medium");
		SMALL_EXPLOSION_CHANCE = (float) this.getConfig().getDouble("explosion_size_chance.small");
		DEFAULT = (float) this.getConfig().getDouble("events.default");
		BLOCK_BREAK = (float) this.getConfig().getDouble("events.block_break");
		BLOCK_PLACE = (float) this.getConfig().getDouble("events.block_place");
		CREATURE_SPAWN = (float) this.getConfig().getDouble("events.creature_spawn");
		PLAYER_JOIN = (float) this.getConfig().getDouble("events.player_join");
		SLEEP_BED = (float) this.getConfig().getDouble("events.sleep_bed");
		PLAYER_DROP_ITEM = (float) this.getConfig().getDouble("events.player_drop_item");
		BUCKET_FILL = (float) this.getConfig().getDouble("events.bucket_fill");
		BUCKET_EMPTY = (float) this.getConfig().getDouble("events.bucket_empty");
		PLAYER_RESPAWN = (float) this.getConfig().getDouble("events.player_respawn");
		ANIMAL_BREED = (float) this.getConfig().getDouble("events.animal_breed");
		ENTITY_TELEPORT = (float) this.getConfig().getDouble("events.entity_teleport");
		PLAYER_CONSUME = (float) this.getConfig().getDouble("events.player_consume");
		TOOL_BREAK = (float) this.getConfig().getDouble("events.tool_break");
		SHEAR_ENTITY = (float) this.getConfig().getDouble("events.shear_entity");
		PLAYER_FISH = (float) this.getConfig().getDouble("events.player_fish");
		THROW_EGG = (float) this.getConfig().getDouble("events.throw_egg");
		EDIT_BOOK = (float) this.getConfig().getDouble("events.edit_book");
		ENCHANT_ITEM = (float) this.getConfig().getDouble("events.enchant_item");
		VEHICLE_DESTROY = (float) this.getConfig().getDouble("events.vehicle_destroy");
		CREATE_PORTAL = (float) this.getConfig().getDouble("events.create_portal");
		BREW_POTION = (float) this.getConfig().getDouble("events.brew_potion");
		ENTITY_DEATH = (float) this.getConfig().getDouble("events.entity_death");
		ENTITY_MOUNT = (float) this.getConfig().getDouble("events.entity_mount");
		ENTITY_SHOOT_BOW = (float) this.getConfig().getDouble("events.entity_shoot_bow");
		SLIME_SPLIT = (float) this.getConfig().getDouble("events.slime_split");
		FIREWORK_EXPLODE = (float) this.getConfig().getDouble("events.firework_explode");
		ITEM_DESPAWN = (float) this.getConfig().getDouble("events.item_despawn");
		VILLAGER_ACQUIRE_TRADE = (float) this.getConfig().getDouble("events.villager_acquire_trade");
		FURNACE_BURN = (float) this.getConfig().getDouble("events.furnace_burn");
		LEAVES_DECAY = (float) this.getConfig().getDouble("events.leaves_decay");
		SIGN_CHANGE = (float) this.getConfig().getDouble("events.sign_change");
		RIGHT_CLICK_BLOCK = (float) this.getConfig().getDouble("events.right_click_block");
		PLAYER_TAKE_LECTERN_BOOK = (float) this.getConfig().getDouble("events.player_take_lectern_book");
		PLAYER_LEASH_ENTITY = (float) this.getConfig().getDouble("events.player_leash_entity");
	}
}
