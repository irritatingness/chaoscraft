package com.irritatingness;

import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

public class ExplosionTimedRunnable implements Runnable {

    private Location loc;
    private float explosion;

    public ExplosionTimedRunnable(Location loc, float explosion) {
        this.loc = loc;
        this.explosion = explosion;
    }

    @Override
    public void run() {
        loc.getWorld().createExplosion(loc, explosion, false, true);
    }
}
